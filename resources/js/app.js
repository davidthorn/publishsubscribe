$( function(){
	$( document ).ready( function(){

		var socket = io();

		var pubnub = new PubNub({
		    subscribeKey: "subscribeKey",
		    publishKey: "publishKey",
		    ssl: true
		});


		pubnub.addListener({
		    status: function(statusEvent) {
		        if (statusEvent.category === "PNConnectedCategory") {
		            var payload = {
		                my: 'my Main message'
		            };
		            pubnub.publish(
		                { 
		                    message: payload,
		                    channel: "Channel-z41ql4qsy"
		                }, 
		                function (status) {
		                    // handle publish response
		                }
		            );
		        }
		    },
		    message: function(object) {
		        // handle message
		        console.log(object);
		        $('.wrapper').append( aLine('david@nodejs' , '~/david' , 'Message Received: ' + object.message.text) );
		    },
		    presence: function(presenceEvent) {
		        // handle presence
		        console.log( presenceEvent );
		    }
		});
		 
		pubnub.subscribe({
		    channels: ['Channel-z41ql4qsy']
		});


	});

} );




function aLine( person , path , cmd )
{
	return '<p><span class="user">' + person + '</span><span class="path"><span>:</span>' + path + '</span>$ <span class="cmd">' + cmd +'</span></p>'; 
};

