
var express = require('express')
var app = express()
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');
var config = require('./Config');

app.set('view engine' , 'jade')

app.use(express.static('resources'))
app.set('views', './views')

http.listen( config().port() , config().host() ,function(){
    console.log( "listening" );
});



app.get('/', function (req, res) {
	res.render('index', { title: 'Hey', message: 'Hello there!' })
})

/*
app.get('*', function (req, res) {
	console.log( req.url );
  	return fs.readFileSync( __dirname . req.url  )
})
*/

io.on('connection' , function(socket){
	console.log( "connection to socket server with id of " + socket.id );

	socket.on('time-please', function(){
		console.log("someone asked what the time is");
		io.sockets.emit('time' ,  new Date().toLocaleTimeString() );
	})

});

